#include "Vector.h"
#include <iostream>

Vector::Vector(int n)
{
	if (n < 2) {
		n = 2;
	}
	this->_elements = new int[n];
	this->capacity = n;
	this->size = 0;
	this->_resizeFactor = n;



}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;

}

int Vector::size() const
{
	return this->size;
}

int Vector::capacity() const
{
	return this->capacity;
}

int Vector::resizeFactor() const
{
	return this->resizeFactor;
}

bool Vector::empty() const
{
	if (this->size != 0) {
		return true;
	}
	return false;
}

void Vector::push_back(const int& val)
{
	int i = 0;
	if (this->_size < this->_capacity) {
		this->_elements[this->_size] = val;
		this->_size++;
		
	}
	else if (this->_size == this->_capacity) {
		Vector v(this->_capacity + this->_resizeFactor);
		for (i = 0; i < this->_size; i++) {
			v._elements[i] = this->_elements[i];
		}
		v._elements[this->_size++] = val;
		v._size = this->_size;
		
		delete[] this->_elements;
		this->_elements = v._elements;






	}



}

int Vector::pop_back()
{	
	int val = 0;
	if (this->_size != 0) {
		val = this->_elements[this->_size];
		this->_size--;
		return val;

	}
	else {
		std::cout << "error: pop from empty vector" << std::endl;
		return ERROR;
	}


}

void Vector::reserve(int n)
{
	if (n < this->_capacity) {
		while (this->_capacity < n) {
			this->_capacity += this->_resizeFactor;
		}
		int* pArr = new int[this->_capacity];
		for (i = 0; i < this->_size ; i++) {
			pArr[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = pArr;


	}


}

void Vector::resize(int n)
{
	int i = 0;
	if (n <= this->_capacity) {
		int* pArr = new int[n];
		for (i = 0; i < n; i++) {
			pArr[i] = this->_elements[i];
		}
		for (i = this->_size; i < n; i++) {
			pArr[i] = 0;
		}
		this->_size = n;
		delete[] this->_elements;
		this->_elements = pArr;
	}
	else {
		this->reserve(n);
		for (i = this->_size; i < this->_capacity; i++) {
			this->_elements[i] = 0;
		}
		this->_size = n;



	}

}

void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}

Vector& Vector::operator=(const Vector& other)
{
	delete[] this->_elements; // release old memory

	// shallow copy fields
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;

	// deep copy dynamic fields (pointers/arrays)
	this->_elements = new int[_size];
	for (int i = 0; i < _size; i++)
	{
		// copies cell by cell
		this->_elements[i] = other._elements[i];
	}

	// TODO: insert return statement here
	return *this;
}

int& Vector::operator[](int n) const
{
	if (n > this->_size || n < 0)
	{
		std::cout << "ERROR:print the first value:" + _elements[0] << std::endl;
		return this->_elements[0];
	}
	else
	{
		return this->_elements[n];
	}
}
